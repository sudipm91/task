<header class="mainheader">
  <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav mainNav">
              <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Faculties <span class="caret"></span>
                </a>
                 @if(!empty($faculties))
                                    <?php 
                                    $faculties = \App\Faculty::all(); 
                                    $faculties = $faculties->toArray();
                                    $chunked_categories = array_chunk($faculties, 3);
                                    ?>
                        <div class="categorydropmenu">
                                <ul class="dropdown-menu">
                                    <div class="row">
                                        @foreach($chunked_categories as $faculties)
                                            <div class="submenuDiv col-md-3 col-sm-3">
                                                <li>
                                                    <ul>
                                                          @foreach($faculties as $faculty)
                                                              <?php $subfaculty = \App\SubFaculty::where('faculty_id', $faculty['id'])->first();?>
                                                         
                                                         <li><a style="text-decoration:none"  href="{{route('showLecturer')}}?faculty={{strtolower($faculty['faculty_name'])}}&subfaculty={{strtolower($subfaculty['subfaculty_name'])}}">{{ucfirst($faculty['faculty_name'])}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                    
                                                </li>
                                            </div>
                                            @endforeach
                                            @endif
                                    </div>
                                </ul>
                            </div>
            </li>         
              <li><a href="{{url('/')}}">Home</a></li>
            
          </ul> 
    
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
  </nav>
</header>
<!-- header part ended here -->