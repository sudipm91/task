@if (count($errors))

<div class="cnw">
  <section class="cnr">
    <div class="row">
      <div class="col-lg-12">

        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

          <h4>  <i class="icon fa fa-check"></i> Alert!</h4>
          @foreach ($errors->all() as $error)
          <h5> {{$error}}</h5>
          @endforeach
        </div>
      </div>
    </div>
  </section>
</div>

@endif


<style type="text/css">
.cnw{
background-color:#ecf0f5; margin-left:230px; z-index: 800; 
}
.cnr{
   padding:10px 15px 0 15px;
    margin-right: auto;
    margin-left: auto;
    padding-left: 15px;
    padding-right: 15px;
}
  
</style>