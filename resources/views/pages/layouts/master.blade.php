<!DOCTYPE html>
<html lang="en">
<title>@yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/css/bootstrap.min.css" type='text/css' rel="stylesheet" media="screen">
<link href="/css/animations.css" type='text/css' rel="stylesheet">
<link href="/css/font-awesome.min.css" type='text/css' rel="stylesheet">
<link href="/css/responsive.css" type='text/css' rel="stylesheet">
<link href="/css/style.css" type='text/css' rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,‌​100italic,300,300ita‌​lic,400italic,500,50‌​0italic,700,700itali‌​c,900italic,900' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet"> 
<link rel='stylesheet' id='googleFonts-css'  href='https://fonts.googleapis.com/css?family=Exo+2%3A400%2C800%2C900%2C700%2C600%2C500%2C300%2C200%2C100%7COpen+Sans%3A400%2C700%2C600%2C300%2C300italic%2C400italic%2C600italic%2C700italic%2C800%2C800italic' type='text/css' media='all' />
</head>
<body>


 @include('pages.layouts.header')

 @yield('content')




<script src='/js/jquery-3.1.1.min.js'></script>
<script src='/js/bootstrap.min.js'></script>
<script type="text/javascript">
    /***************** show and hide header ******************/
    $(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".mainheader").addClass("showheader");
    } else {
        $(".mainheader").removeClass("showheader");
    }
    });
</script>
<script type="text/javascript">
    $('ul.nav li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown("slideDown");
    }, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp("slideUp");
    });
</script>
@yield('scripts')
</body>
</html>