@extends ('pages.layouts.master')
@section('title')
TMS
@endsection
@section('content')

<section class="container" style="padding-top: 120px;">
    <div class="container-fluid">
          <div class="row" style="padding: 60px 0 40px 0">
        <div class="col-md-12">
            <h2 style="color: #039cd8;padding: 20px;text-align: center;text-transform: uppercase;">Teacher Management System</h2>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12"><h4 class="h5">{{isset($_GET['faculty']) ? ucwords($_GET['faculty']) : ""}} {{isset($_GET['subfaculty']) ? " : " .ucwords( $_GET['subfaculty']) : ""}}</h4></div>
            <div class="col-md-3">
                <div class="sub_cat">
                    <h3 class="h3">Sub-categories</h3>
                    <form class="subcat_form">
                        <div class="form-check">
                                @foreach($subfaculties as $subfaculty)
                                <li><a href="show?faculty=<?=$_GET['faculty'];?>&subfaculty={{strtolower($subfaculty->subfaculty_name)}}">{{ucfirst($subfaculty->subfaculty_name)}} ({{App\Lecturer::where('sub_faculty_id', $subfaculty->id)->count()}})</a></li>  
                            @endforeach
                        </div>
                    </form>    
                </div>
            </div>
            <div class="col-md-9">
                <div class="row cm10-row">
                     @foreach($searched_lecturers as $searched_lecturer)
                    <div class="col-md-4">
                        <div class="mini-block">
                            <div class="row cm-row">
                                
                                <div class="col-md-12">
                                    <div class="all10p">
                                        <h2 class="miniprof_title">
                                            LECTURER DETAIL
                                        </h2>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="company_name">
                                {{ucwords($searched_lecturer['name'])}}
                            </div>
                            <div class="client_contact">
                                <p><span class="fa fa-map-marker"></span>{{ucwords($searched_lecturer['address'])}}</p>
                                <p><span class="fa fa-envelope"></span>{{$searched_lecturer->email}}</p>
                                <p><span class="fa fa-phone"></span>{{$searched_lecturer->phone}}</p>
                                <p><span class="fa fa-birthday-cake"></span>{{$searched_lecturer->dob}}</p>
                                <p><span class="fa fa-user"></span>{{ucwords($searched_lecturer['nationality'])}}</p>
                                <p><span class="fa fa-user"></span> @if($searched_lecturer->gender == 'm')
                       {{"Male"}}
                  @elseif($searched_lecturer->gender == "f")
                       {{"Female"}} <br>
                      @else
                       {{"Other"}} <br>
                            @endif</p>
                                
                                <!-- <div class="likebtn">
                                    <span class="counting" dir="rtl">0</span>
                                    <span class="fa fa-thumbs-up"></span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                {!!$searched_lecturers->appends(Input::except('page'))->links()!!}
            </div>
        </div>
    </div>
</section>
<!-- featured block ended here -->

@endsection   
