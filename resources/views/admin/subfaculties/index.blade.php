@extends('admin.adminlayouts.admaster')
@section('title')
Admin | subfaculty
@endsection
@section('content')
@include('pages.layouts.message')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      subfaculty Table
    </h1>
    <ol class="breadcrumb">
      <li><a href="#">subfaculty Table</a></li>
      <li class="active">Data table</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <a href="{{route('subfaculties.create')}}"  class="btn btn-primary pull-right" style="margin-right: 20px;margin-bottom: 10px;">  <i class="fa fa-plus" style="padding-right:5px;"></i>Create</a>
      <div class="col-xs-12 ">
        <!-- /.box-header -->
        <!-- /.box -->
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title text-primary">Total subfaculty : {{count($subfaculties)}}</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>Subfaculty</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($subfaculties as $subfaculty)
                <tr>
                  <td> {{$loop->index+1}}</td>
                  <td> {{$subfaculty->subfaculty_name}}</td>
                      <td>
                       
                        <a class='btn btn-primary btn-xs'  style="margin-left:5px;" href="{{'/admin/subfaculties/'.$subfaculty->id.'/edit'}}"><i class="glyphicon glyphicon-edit"></i></a>
                        <form class="form-group" style="display: inline;"  action="{{'/admin/subfaculties/'.$subfaculty->id}}" onsubmit="return confirmAction('Are you sure you want to delete?')" method="post">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete ImageFolder" style="margin-left:5px;"><i class="glyphicon glyphicon-trash"></i></button>
                      </form>
                      </td>
                    </tr>
                    @endforeach 
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->

    <!-- DataTables -->
@endsection