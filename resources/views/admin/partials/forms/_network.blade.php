<?php 
$network_available = isset($network) ? true : false; 
?>
{{csrf_field()}}


    <div class="form-group">
        <label for="branch_name">Branch Name:</label>
        <input type="text" class="form-control" name="branch_name" id="branch_name" placeholder="Eg. Dhankuta Branch" value="{{ $network_available ? $network->branch_name : old('branch_name')}}" >
        <span class="error-display"><i style='color: red;'> {!! $errors->first('branch_name') !!} </i></span>
    </div>

    <div class="form-group">
        <label for="photo">Choose Image:</label>
        <input type="file" name="photo" id="photo"  value="{{ $network_available ? $network->photo : old('photo')}}" />
    </div>

     <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" class="form-control" name="name" id="name" placeholder="Eg. Sudip Maharjan" value="{{ $network_available ? $network->name : old('name')}}" >
        <span class="error-display"><i style='color: red;'> {!! $errors->first('name') !!} </i></span>
    </div>

     <div class="form-group">
        <label for="position">Position:</label>
        <input type="text" class="form-control" name="position" id="position" placeholder="Eg. Branch Incharge" value="{{ $network_available ? $network->position : old('position')}}" >
        <span class="error-display"><i style='color: red;'> {!! $errors->first('position') !!} </i></span>
    </div>

      <div class="form-group">
        <label for="branch_address">Branch Address:</label>
        <input type="text" class="form-control" name="branch_address" id="branch_address" placeholder="Eg. MangalBazar, lalitpur" value="{{ $network_available ? $network->branch_address : old('branch_address')}}" >
        <span class="error-display"><i style='color: red;'> {!! $errors->first('branch_address') !!} </i></span>
    </div>

     <div class="form-group">
        <label for="phone">Phone Number:</label>
        <input type="text" class="form-control" name="phone" id="phone" placeholder="Eg. 9868193434, 01-5531692" value="{{ $network_available ? $network->phone : old('phone')}}" >
        <span class="error-display"><i style='color: red;'> {!! $errors->first('phone') !!} </i></span>
    </div>


    <div class="form-group">
        <label for="email">Branch E-mail:</label>
        <input type="email" name="email" class="form-control" id="email" placeholder="Eg. helloworld@gmail.com" value="{{$network_available ? $network->email : old('email')}}">
        <span class="error-display"><i style='color: red;'> {!! $errors->first('email') !!} </i></span>
    </div>

    <div class="form-group">
        <label for="facebook">Facebook:</label>
        <input type="text" name="facebook" class="form-control" id="facebook" placeholder="Eg. https://www.facebook.com/Sudip.Maharjan" value="{{$network_available ? $network->facebook : old('facebook')}}">
        <span class="error-display"><i style='color: red;'> {!! $errors->first('facebook') !!} </i></span>
    </div>

    <div class="form-group">
        <label for="twitter">Twitter:</label>
        <input type="text" name="twitter" class="form-control" id="twitter" placeholder="Eg. https://twitter.com/manakamana12" value="{{$network_available ? $network->twitter : old('twitter')}}">
        <span class="error-display"><i style='color: red;'> {!! $errors->first('twitter') !!} </i></span>
    </div>

    <div class="form-group">
        <label for="gmail">Gmail:</label>
        <input type="text" name="gmail" class="form-control" id="gmail" placeholder="Eg. https://plus.google.com/u/0/105980531774762804490" value="{{$network_available ? $network->gmail : old('gmail')}}">
        <span class="error-display"><i style='color: red;'> {!! $errors->first('gmail') !!} </i></span>
    </div>

    <div class="form-group">
    <label for="order">Order:</label>
    <input type="number" class="form-control" name="order" row="5" id="order"   value="{{ $network_available ? $network->order : old('order')}}" placeholder="1">
    <span class="error-display"><i style='color: red;'> {!! $errors->first('order') !!} </i></span>
</div>

<br/>



    

