<?php 
$subfaculty_available = isset($subfaculty) ? true : false; 
?>
{{csrf_field()}}
<div class="form-group">
    <label for="subfaculty_name">Sub Faculty:</label>
    <input class="form-control"   name="subfaculty_name"  value="{{$subfaculty_available ? $subfaculty->subfaculty_name : old('subfaculty_name')}}" id="subfaculty_name" type="text">
    <span class="error-display"><i style='color: red;'>  {!! $errors->first('subfaculty_name') !!} </i></span>
</div>

<div class="form-group"> 
    <label for="faculty_id">Select a faculty</label>
    <select id="faculty_id" name="faculty_id" id="faculty_id" class="form-control" required>
        <option value="">Select  faculty</option>
        @foreach($faculties as $faculty)
        <option value="{{$faculty->id}}" {{$subfaculty_available && ($faculty->id == $subfaculty->faculty_id) ? "selected = 'selected'" : ""}}>{{$faculty->faculty_name}}</option>
        @endforeach
    </select>
</div>










