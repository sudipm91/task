<?php 
$lecturer_available = isset($lecturer) ? true : false; 
?>
{{csrf_field()}}


<div class="form-group">
	<label for="name">Full Name*</label>
	<input type="text" class="form-control" name="name" id="name" placeholder="Eg. Priti Sharma" value="{{ $lecturer_available ? $lecturer->name : old('name')}}" >
	<span class="error-display"><i style='color: red;'> {!! $errors->first('name') !!} </i></span>
</div>

<div class="form-group">
	<label for="phone">Phone Number*</label>
	<input type="number" name="phone" class="form-control" id="phone" placeholder="Eg. 123456789" value="{{$lecturer_available ? $lecturer->phone : old('phone')}}" >
	<span class="error-display"><i style='color: red;'> {!! $errors->first('phone') !!} </i></span>
</div>

<div class="form-group">
	<label for="email">Email address</label>
	<input type="email" name="email" class="form-control" id="email" placeholder="Eg. miss_sharma@gmail.com" value="{{$lecturer_available ? $lecturer->email : old('email')}}">
	<span class="error-display"><i style='color: red;'> {!! $errors->first('email') !!} </i></span>
</div>

<div class="form-group">
	<label for="address">Address*</label>
	<input type="text" class="form-control" name="address" id="address" placeholder="Eg. Chabahil, Kathmandu" value="{{ $lecturer_available ? $lecturer->address : old('address')}}" >
	<span class="error-display"><i style='color: red;'> {!! $errors->first('address') !!} </i></span>
</div>

<div class="form-group">
	<label for="nationality">Nationality*</label>
	<input type="text" class="form-control" name="nationality" id="nationality" placeholder="Eg. Chabahil, Kathmandu" value="{{ $lecturer_available ? $lecturer->nationality : old('nationality')}}" >
	<span class="error-display"><i style='color: red;'> {!! $errors->first('nationality') !!} </i></span>
</div>

<div class="form-group">
	<label for="email">Date of Birth*</label>
	<input type="text" name="dob" class="form-control" id="dob" placeholder="Eg. 2055/01/17" value="{{$lecturer_available ? $lecturer->dob : old('dob')}}" >
	<p class="help-block">Date Format: yy/mm/dd</p>
	<span class="error-display"><i style='color: red;'> {!! $errors->first('dob') !!} </i></span>
</div>

<div class="form-group">
	<div class="radio">
		<p><strong>Gender*</strong></p>
		<label>
			<input type="radio"  name="gender" id="male" value="m" {{old('gender') == "m" ? "checked" : ''}} {{($lecturer_available && $lecturer->gender == 'm') ? 'checked' : ''}}>
			Male
		</label>
	</div>
	<div class="radio">
		<label>
			<input type="radio" name="gender" id="female" value="f" {{old('gender') == "f" ? "checked" : ''}} {{($lecturer_available && $lecturer->gender == 'f') ? 'checked' : ''}}>
			Female
		</label>
	</div>
	<div class="radio">
		<label>
			<input type="radio" name="gender" id="other" value="o" {{old('gender') == "o" ? "checked" : ''}} {{($lecturer_available && $lecturer->gender == 'o') ? 'checked' : ''}}>
			Other
		</label>
	</div>
</div>



<div class="form-group"> 
	<label for="faculty_id">Select a faculty</label>
	<select id="faculty_id" name="faculty_id"  class="form-control" required>
		<option value="">Select  faculty</option>
		@foreach($faculties as $faculty)
		<option value="{{$faculty->id}}" {{$lecturer_available && ($faculty->id == $lecturer->faculty_id) ? "selected = 'selected'" : ""}}>{{$faculty->faculty_name}}</option>
		@endforeach
	</select>
</div>

<div class="form-group"> 
	@if($lecturer_available)
	<?php $subfaculty = App\SubFaculty::find($lecturer->sub_faculty_id);?>
	@endif
	<label for="sub_faculty_id">Select Subfaculty</label>
	<select id="sub_faculty_id" name="sub_faculty_id" class="form-control" required>
		<option value="{{$lecturer_available ? (int)$lecturer->sub_faculty_id : old('sub_faculty_id')}}">{{$lecturer_available ? $subfaculty->subfaculty_name : "Select Subfaculty"}} </option>
	</select>
</div>

@section('scripts') 
<script>

	$('#faculty_id').on('change', function(e){
		console.log(e);

		var cat_id = e.target.value;
		$.get('/admin/ajax-subcat?cat_id='+cat_id, function(data){
			$('#sub_faculty_id').empty();
			$('#sub_faculty_id').append('<option>Select SubFaculty</option>');
			$.each(data, function(index, subcatObj){
				$('#sub_faculty_id').append('<option value="'+subcatObj.id+'">'+subcatObj.subfaculty_name+'</option>')
			});
		});
	});

</script>
@endsection




