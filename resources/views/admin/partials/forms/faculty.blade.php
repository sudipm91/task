<?php 
$faculty_available = isset($faculty) ? true : false; 
?>
{{csrf_field()}}
<div class="form-group">
    <label for="faculty_name">Faculty:</label>
    <input class="form-control"   name="faculty_name"  value="{{$faculty_available ? $faculty->faculty_name : old('faculty_name')}}" id="faculty_name" type="text">
    <span class="error-display"><i style='color: red;'>  {!! $errors->first('faculty_name') !!} </i></span>
</div>










