@extends('admin.adminlayouts.admaster')
@section('title')
faculty | Create
@endsection
@section('content')

@include('pages.layouts.message')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            faculty Table
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">faculty Table</a></li>
            <li class="active">Data table</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <a href="{{route('faculties.index')}}" class="btn btn-primary pull-right" style="margin-right: 20px;margin-bottom: 10px;">Back</a>
            <div class="col-xs-12">

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">faculty Table</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-8 col-md-offset-2" > 
                       
                            <form class="form form-validate" method="post"  action="{{route('faculties.store')}}">
                                @include('admin.partials.forms.faculty')
                                <div class="form-group">
                                    <div class="box-footer">
                                        <input class="btn btn-primary pull-right" type="submit" value="Create">
                                        <input class="btn btn-danger pull-left" type="reset" value="Reset">
                                    </div>  
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@endsection