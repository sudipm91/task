@extends('admin.adminlayouts.admaster')
@section('title')
Admin | lecturer
@endsection
@section('content')
@include('pages.layouts.message')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      lecturer Table
    </h1>
    <ol class="breadcrumb">
      <li><a href="#">lecturer Table</a></li>
      <li class="active">Data table</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <a href="{{route('lecturers.create')}}"  class="btn btn-primary pull-right" style="margin-right: 20px;margin-bottom: 10px;">  <i class="fa fa-plus" style="padding-right:5px;"></i>Create</a>
      <div class="col-xs-12 ">
        <!-- /.box-header -->
        <!-- /.box -->
        <div class="box box-info">
          <div class="box-header">
            <h3 class="box-title text-primary">Total lecturer : {{count($lecturers)}}</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>S.N</th>
                  <th>lecturer</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th>Nationality</th>
                  <th>DOB</th>
                  <th>Gender</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($lecturers as $lecturer)
                <tr>
                  <td>{{$loop->index+1}}</td>
                  <td>{{$lecturer->name}}</td>
                  <td>{{$lecturer->email}}</td>
                  <td>{{$lecturer->phone}}</td>
                  <td>{{$lecturer->address}}</td>
                  <td>{{$lecturer->nationality}}</td>
                  <td>{{$lecturer->dob}}</td>
                  <td>   @if($lecturer->gender == 'm')
                       {{"Male"}}
                  @elseif($lecturer->gender == "f")
                       {{"Female"}} <br>
                      @else
                       {{"Other"}} <br>
                            @endif
                      <br></td>
                      <td>
                        <a class='btn btn-info btn-xs'  style="margin-left:5px;" href="{{route('lecturers.show', $lecturer)}}"><i class="fa fa-eye"></i></a>
                        <a class='btn btn-primary btn-xs'  style="margin-left:5px;" href="{{route('lecturers.edit', $lecturer->id)}}"><i class="glyphicon glyphicon-edit"></i></a>
                        <form class="form-group" style="display: inline;"  action="{{'/admin/lecturers/'.$lecturer->id}}" onsubmit="return confirmAction('Are you sure you want to delete?')" method="post">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete ImageFolder" style="margin-left:5px;"><i class="glyphicon glyphicon-trash"></i></button>
                      </form>
                      </td>
                    </tr>
                    @endforeach 
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->

    <!-- DataTables -->
@endsection