@extends('admin.adminlayouts.admaster')
@section('title')
{{$lecturer->lecturer_name}}
@endsection
@section('content')
@include('pages.layouts.message')

<div class="content-wrapper">
	<section class="content-header">
		<h1>
			{{$lecturer->lecturer_name}}
		</h1>
		<ol class="breadcrumb">
			<li><a href="#">lecturer Table</a></li>
			<li class="active">Data table</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box-header pull-right">
					<a href="{{route('lecturers.index')}}" class="btn btn-primary btn-sm btn-flat" >Back</a>
					<a class="btn btn-primary btn-sm btn-flat" href="{{route('lecturers.create')}}">
						<i class="fa fa-plus"></i> Add lecturer
					</a>
					<a class="btn btn-primary btn-sm btn-flat" href="{{route('lecturers.edit', $lecturer->id)}}">
						<i class="fa fa-edit"></i> Edit lecturer
					</a>
				</div>
			</div>
			<div class="col-xs-12">
				<!-- /.box-header -->
				<!-- /.box -->
				<div class="box box-info">
					<div class="box-header">
						<h3 class="box-title">{{$lecturer->lecturer_name}}</h3>
					</div>
					<br/>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="col-md-8" >              
							<p><strong>ID</strong><br> {{$lecturer->id}}<br></p>
							<p class="c"><strong>Full Name</strong><br>{{$lecturer->name}} <br></p>
							<p class="c"><strong>Address</strong><br>{{$lecturer->address}} <br></p>
							<p class="c"><strong>Nationality</strong><br>{{$lecturer->nationality}} <br></p>
							<p class="c"><strong>Date Of Birth</strong><br>{{$lecturer->dob}} <br></p>
							<p><strong>Phone</strong><br>{{$lecturer->phone}} <br></p>
							<p><strong>Email</strong><br>{{$lecturer->email}} <br></p>
							<p><strong>Gender</strong><br>@if($lecturer->gender == 'm')
								{{"Male"}}
								@elseif($lecturer->gender == "f")
								{{"Female"}} <br>
								@else
								{{"Other"}} <br>
							@endif</p>
							<br/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
@endsection