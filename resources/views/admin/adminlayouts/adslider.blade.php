<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <ul class="sidebar-menu" data-widget="tree">

      <li>
        <a href="{{route('admin.dashboard')}}">
          <i class="fa fa-dashboard fa-lg"></i> <span>Dashboard</span>
        </a>
      </li>

      <li>
        <a href="{{route('faculties.index')}}">
          <i class="fa fa-book"></i> <span>Faculty</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>

      <li>
        <a href="{{route('subfaculties.index')}}">
          <i class="fa fa-book"></i> <span>Sub faculty</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>

       <li>
        <a href="{{route('lecturers.index')}}">
          <i class="fa fa-book"></i> <span>Lecturer</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>