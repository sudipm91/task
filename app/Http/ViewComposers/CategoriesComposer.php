<?php
namespace App\Http\ViewComposers;
use Illuminate\View\View;
use App\Faculty;
class CategoriesComposer
{
    public $faculties;

    public function __construct()
    {
        $this->faculties =  Faculty::all()->chunk(4);
    }

    public function compose(View $view)
    {
        $view->with('faculties', $this->faculties);
    }

}