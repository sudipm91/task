<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lecturer;
use App\Faculty;
use App\SubFaculty;
use Excel;
use App\Exports\DataExport;

class LecturersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $lecturers = Lecturer::all();
       return view('admin.lecturers.index',compact('lecturers'));
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $faculties = Faculty::all();
        $subfaculties = SubFaculty::all();
        return view('admin.lecturers.create',compact('faculties','subfaculties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     $this->validate($request, [
        'name' =>'required|min:5',
        'address' =>'required',
        'email' =>'required',
        'nationality' =>'required',
        'gender' =>'required',
        'dob' =>'required',
    ]);

     $lecturer = new Lecturer;
     $lecturer->name = $request->name;
     $lecturer->phone = $request->phone;
     $lecturer->address = $request->address;
     $lecturer->email = $request->email;
     $lecturer->nationality = $request->nationality;
     $lecturer->dob = $request->dob;
     $lecturer->gender = $request->gender;
     $lecturer->faculty_id = $request->faculty_id;
     $lecturer->sub_faculty_id = $request->sub_faculty_id;
     $lecturer->save();
     session()->flash('message','Lecturer Added Successfully');
     return redirect()->route('lecturers.index');
 }

 public function getSubfaculties(Request $request)
 {
    return SubFaculty::where('faculty_id', $request->cat_id)->get();
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $faculties = Faculty::all();
        $subfaculties = SubFaculty::all();
        $lecturer = Lecturer::find($id);
        return view('admin.lecturers.show',compact('lecturer','faculties','subfaculties'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faculties = Faculty::all();
        $subfaculties = SubFaculty::all();
        $lecturer = Lecturer::find($id);
        return view('admin.lecturers.edit',compact('lecturer','faculties','subfaculties'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [

       ]);

       $lecturer = Lecturer::find($id);
       $lecturer->name = $request->name;
       $lecturer->phone = $request->phone;
       $lecturer->address = $request->address;
       $lecturer->email = $request->email;
       $lecturer->nationality = $request->nationality;
       $lecturer->dob = $request->dob;
       $lecturer->gender = $request->gender;
       $lecturer->faculty_id = $request->faculty_id;
       $lecturer->sub_faculty_id = $request->sub_faculty_id;
       $lecturer->save();
       session()->flash('message','Lecturer Updated Successfully');
       return redirect()->route('lecturers.index');
   }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Lecturer = Lecturer::find($id);
        $Lecturer->delete();
        session()->flash('message','Lecturer Deleted Successfully');
        return redirect()->route('lecturers.index');
    }

     public function add()
   {
    return view('lecturer');
   }


    public function exportFile()
     {
   return Excel::download(new DataExport, 'data.csv');
     }

}
