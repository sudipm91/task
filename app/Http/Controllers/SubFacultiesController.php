<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubFaculty;
use App\Faculty;

class SubFacultiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $subfaculties =  SubFaculty::all();
          return view('admin.subfaculties.index', compact('subfaculties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $faculties = Faculty::all();
        return view('admin.subfaculties.create',compact('faculties'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subfaculty = new SubFaculty;
        $this->validate($request,[
            'subfaculty_name'=> 'required',
        ]);
            
        $subfaculty->subfaculty_name = $request->subfaculty_name;
        $subfaculty->faculty_id = $request->faculty_id;
        $subfaculty->save();
        session()->flash('message','Subfaculty Added Successfully');
        return redirect()->route('subfaculties.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faculties = Faculty::all();
        $subfaculty = SubFaculty::find($id);
        return view('admin.subfaculties.edit',compact('faculties','subfaculty'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $subfaculty = SubFaculty::find($id);
        $this->validate($request,[
            
        ]);
            
        $subfaculty->subfaculty_name = $request->subfaculty_name;
        $subfaculty->faculty_id = $request->faculty_id;
        $subfaculty->save();
        session()->flash('message','Subfaculty Updated Successfully');
        return redirect()->route('subfaculties.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subfaculty = SubFaculty::find($id);
        $subfaculty->delete();
        session()->flash('message','Subfaculty Deleted Successfully');
        return redirect()->route('subfaculties.index');
    }
}
