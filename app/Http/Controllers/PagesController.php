<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faculty;
use App\SubFaculty;
use App\Lecturer;
class PagesController extends Controller
{
   public function index()
    { 
    	 
    	$faculties = Faculty::all();
        return view('pages.index', compact('faculties'));
       
    }

    public function show_lecturer(Request $request)
    {
        
    $faculties = Faculty::all();
    $faculty = strtolower($request->faculty);
    $faculty = Faculty::where('faculty_name', $faculty)->first();
    $faculty_id = $faculty->id;
    $subfaculties = SubFaculty::where('faculty_id', $faculty_id)->orderBy('subfaculty_name', 'asc')->get();
    $subfaculty = SubFaculty::where('subfaculty_name', $request->subfaculty)->first();
    $searched_lecturers = Lecturer::where('sub_faculty_id', $subfaculty->id)->orderBy('created_at', 'desc')->paginate(12);
        return view('pages.show_page',compact('faculties','subfaculties','searched_lecturers'));
    
    }
}
