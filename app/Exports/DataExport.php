<?php

namespace App\Exports;
use App\Lecturer;

use Maatwebsite\Excel\Concerns\FromCollection;

class DataExport implements FromCollection
{
    public function collection()
    {
        return Lecturer::all();
    }
}