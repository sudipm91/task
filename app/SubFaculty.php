<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubFaculty extends Model
{
     public function faculty()
    {
       return $this->belongsToMany(Faculty::class);
    }
}
