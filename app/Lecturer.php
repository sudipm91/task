<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
     public function faculties()
    {
        return $this->belongsToMany(Faculty::class);
    }
}
