<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
   public function subfaculties()
   {
  return $this->hasMany(SubFaculty::class,'faculty_id');
  }

   
    public function lecturers()
    {
        return $this->belongsToMany(Lecturer::class);
    }
}
