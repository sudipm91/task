<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



  Route::get('/',
    [
      'uses' => 'PagesController@index',
      'as' => 'front'
    ]
  );
 Route::get('/show', ['uses'=>'PagesController@show_lecturer', 'as'=>'showLecturer']);


Route::group(['prefix' => 'admin'], function(){

Route::get('dashboard', [
            'uses' => 'AdminController@dashboard',
            'as' => 'admin.dashboard'
        ]);
Route::resource('faculties', 'FacultiesController');
Route::resource('subfaculties', 'SubFacultiesController');
Route::resource('lecturers', 'LecturersController');
Route::get('ajax-subcat', ["uses" => "LecturersController@getSubfaculties"]);
Route::get('lecturer', 'LecturersController@add');
Route::get('lecturer/export', 'LecturersController@exportFile');
    });
